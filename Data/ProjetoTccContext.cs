﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ProjetoTcc.Data
{
    public class ProjetoTccContext : IdentityDbContext
    {
        public ProjetoTccContext(DbContextOptions<ProjetoTccContext> options)
            : base(options)
        {
        }
    }
}
