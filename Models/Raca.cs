﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoTcc.Models
{
    public class Raca
    {
        public int RacaId { get; set; }
        [MaxLength(200, ErrorMessage = "O limite do campo é {0}")]
        [Display(Name = "Raça")]
        public string NomeRaca { get; set; }
        public virtual ICollection<Animal> Animais { get; set; }
    }
}
