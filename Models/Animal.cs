﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoTcc.Models
{
    public class Animal
    {
        public Guid AnimalId { get; set; }
        [MaxLength(200, ErrorMessage = "O limite do campo {1} é {0}")]
        [Display(Name = "Nome do animal")]
        public string Nome { get; set; }
        [MaxLength(200, ErrorMessage = "O limite do campo é {0}")]
        public string Cor { get; set; }
        [Display(Name = "Raça")]
        public int RacaId { get; set; }
        [Display(Name = "Porte")]
        public int PorteId { get; set; }
        [ForeignKey("Usuarios")]
        public Guid UsuariosId { get; set; }
        public virtual Usuario Usuarios { get; set; }
        public virtual Raca Racas { get; set; }
        public virtual Porte Portes { get; set; }
    }
}
